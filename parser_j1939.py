from datetime import datetime, time, date, timedelta
import math
import json
import ast

#file = open('/home/maur0/Documents/Agrum/j1939/logs/file.txt')
#file = open('/home/maur0/Documents/Agrum/j1939/logs/log-feb-15_12-18.txt')
#file = open('/home/maur0/Documents/Agrum/j1939/logs/log-feb-15_12-22-prendiendo.txt')
#file = open('/home/maur0/Documents/Agrum/j1939/logs/log-feb-15_12-22.txt')
file = open('/home/maur0/Documents/Agrum/j1939/logs/log-feb-15_12-24-prendiendo.txt')
def decode_j1939(msg, format_msj="HEX"):
	if format_msj == "HEX":
		msg = int(msg,16)
	elif format_msj == "DEC":
		msg = int(msg)

	header = (msg&0xffffffff0000000000000000) >> 64
	temp = msg & 0xffffffffffffffff
	payload = []
	for i in range(0,8):
		a = (0xff<<((7-i)*8) & temp) >> ((7-i)*8)
		payload.append(a)
	pgn = (header & 0x3ffff00)>>8
	return (pgn,payload)

pgn_list = []

EOF = False
while  not EOF:
	data = file.readline().strip().split(' ')
	#print('data',data)
	#['can0', '', '0CFFFF8C', '', '', '[8]', '', '55', '7D', '7D', '7D', '7D', '7D', 'FF', 'FF']
	if data != ['']:
		msg = data[2]
		for i in range(0,ast.literal_eval(data[5])[0]):
			msg += data[7+i]
		pgn,payload = decode_j1939(msg)
		if pgn == 65276:
			fuel_level = payload[1] * 0.4
			pgn_list.append("fuel level {}%".format(fuel_level))
		#else:
		#	pgn_list[pgn] = payload
	else:
		EOF = True
		for i in range(0,len(pgn_list)):
			#print(pgn_list[i]
			print(pgn_list[i])
	
file.close()

