#Libreria Modulo de base de datos
#Actualizacion: 22 Septiembre 2016
#Actualizo: Diego Ochoa

import urllib
import json
import requests

class DataBase(object):
	def __init__(self,url='http://localhost:1337/'):
		self.url = url
		#self.url = 'http://192.168.1.66:1337/'
		self.server_url = 'http://agrorealtime.herokuapp.com'

	def save_data(self,url,data):
		data = urllib.urlencode(data)
		content = urllib.request.urlopen(url=self.url+url, data=data)
		return content.getcode()

	def save_event(self,url,data):
		headers = {'content-type': 'application/json'}
		response = requests.post(self.url+url, data=json.dumps(data), headers=headers)
		return response
#		data = urllib.urlencode(data)
#		content = urllib.request.urlopen(url=self.url+url, data=data)
#		return content.getcode()
	
	def get(self,path='configs'):
		#print self.url+path
		content = urllib.request.urlopen(url=self.url+path)
		return content.read()

	def get_unsync(self,path='medicion/unsynccd '):
		print(self.url+path)
		content = urllib.request.urlopen(url=self.url+path)
		return content.read()

	def sync(self,data,url='medicion/sync'):
		req = urllib.request.Request(self.url+url)
		req.add_header('Content-Type', 'application/json')
		response = urllib.request.urlopen(req, data)
		return response.read()

	def set_sensor(self,data,url='sensor/'):
		data = urllib.urlencode(data)
		content = urllib.request.urlopen(url=self.url+url, data=data)
		return content.getcode()

	def connect_to_DB(self, data=None, url='/'):
		if data != None:
			#data = urllib.urlencode(data)
			req.add_header('Content-Type', 'application/json')
			content = urllib.request.urlopen(url=self.url+url, data=data)
		else:
			content = urllib.request.urlopen(url=self.url+url)
		return content.getcode()