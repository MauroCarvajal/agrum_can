from __future__ import print_function

import can
from datetime import datetime
from libs.DB_old import DataBase

DB = DataBase(url='http://192.168.1.78:1337/api/v1.0/')
#DB = DataBase(url='http://192.168.8.1:1337/api/v1.0/')

if __name__ == "__main__":
	bus = can.interface.Bus(channel='can0', bustype='socketcan_native')
	can_filters = [{"can_id": 0xfefc00, "can_mask": 0x3ffff00}] #0xfefc00 -> pgn 65276 desplazado a la izquierda 8 bits
	bus.set_filters(can_filters)
	#notifier = can.Notifier(bus, [can.Printer()])
	try:
		while True:
			for msg in bus:
				fuel_level = msg.data[0] * 0.4
				#print(msg.data)
				print("fuel level {}%".format(fuel_level))
				data = {"timestamp": datetime.fromtimestamp(msg.timestamp).isoformat()}
				data["time_start"] = data["timestamp"]
				data["type"] = "fuel_level_percentage"
				data["fuel_level"] = fuel_level
				data["state"] = "Combustible"
				print(data)
				response = DB.save_event('events',data)
				print(response)
	except KeyboardInterrupt:
		pass
	
	print("Stopped script")

